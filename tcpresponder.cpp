#include "tcpresponder.h"


TCPResponder::TCPResponder(QByteArray &data, QObject *parent) :
    QObject(parent),
    m_data(data)
{
    m_Server = new QTcpServer(this);

    connect(m_Server, &QTcpServer::newConnection, this, &TCPResponder::onConnectedSlot);

    m_Server->listen(QHostAddress::Any, 1111) ?
        qDebug() << "Server started!" :
        qDebug() << "Failed to start server!";
}

void TCPResponder::onConnectedSlot()
{
    m_Socket = m_Server->nextPendingConnection();

    m_Socket->write(m_data);

    if (!m_Socket->waitForBytesWritten())
    {
        qDebug() << "Data wasn't sent.";
    }

    m_Socket->disconnectFromHost();
    qDebug() << "Data sent!";
}
