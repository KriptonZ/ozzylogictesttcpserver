#include <QCoreApplication>
#include <QFile>
#include "tcpresponder.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QFile file("response.txt");
    if (!file.exists())
    {
        file.open(QIODevice::WriteOnly);
        file.write("Server response!");
        file.close();
    }
    file.open(QIODevice::ReadOnly);
    QByteArray FileData = file.readAll();
    file.close();

    TCPResponder tcpr(FileData);

    return a.exec();
}
