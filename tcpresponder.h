#ifndef TCPRESPONDER_H
#define TCPRESPONDER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

class TCPResponder : public QObject
{
    Q_OBJECT
public:
    explicit TCPResponder(QByteArray &data, QObject *parent = nullptr);
public slots:
    void onConnectedSlot();
private:
    QTcpServer * m_Server;
    QTcpSocket * m_Socket;
    QByteArray &m_data;
};

#endif // TCPRESPONDER_H
